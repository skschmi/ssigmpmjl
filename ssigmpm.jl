

# Library for NURBS
if(!isdefined(:SSNurbsTools))
    include("./ssnurbstoolsjl/nurbstools.jl")
end

include("./igmp_particle.jl")

using SSNurbsTools
import SSNurbsTools.Nurbs
import SSNurbsTools.UniqueKnotArray
import SSNurbsTools.basis
import SSNurbsTools.basis_tensorprod
import SSNurbsTools.basis_ds
import SSNurbsTools.delBasis
import SSNurbsTools.delBasis_tensorprod
import SSNurbsTools.delBasis_ds
import SSNurbsTools.bernstein_basis_vdv
import SSNurbsTools.domain_dXdxi
import SSNurbsTools.domain_X
import SSNurbsTools.ConvertToBernsteinMesh
import SSNurbsTools.RefineMesh
import SSNurbsTools.Jacobian
import SSNurbsTools.Jacobian_ds
import SSNurbsTools.e_xi_for_t
import SSNurbsTools.Multiplicity
import SSNurbsTools.InsertKnot

type IGMPMGrid2D
    grid::Nurbs
    domain::Array{Domain}
    nparts::Int64
    parts::Array{Particle}
    nodes::Array{Node,2}
    material_density::Float64
    g::Array{Float64}
    k_bulkmod::Float64
    c_tait::Float64
    timestep::Float64
    function IGMPMGrid2D(grid,
                         simulation_info,
                         material_info,
                         particles_info)

        self = new()

        # Grid
        self.grid = grid
        uniqueknots_x = UniqueKnotArray(self.grid.knot_v[1])
        uniqueknots_y = UniqueKnotArray(self.grid.knot_v[2])
        n_spatial_elements_x = length(uniqueknots_x)-1
        n_spatial_elements_y = length(uniqueknots_y)-1
        self.domain = Array{Domain}(2)
        self.domain[1] = Domain(0.,0.)
        self.domain[2] = Domain(0.,0.)
        self.domain[1].min = minimum(self.grid.control_pts[1,:,:]) + (uniqueknots_x[2] - uniqueknots_x[1])
        self.domain[1].max = maximum(self.grid.control_pts[1,:,:]) - (uniqueknots_x[end] - uniqueknots_x[end-1])
        self.domain[2].min = minimum(self.grid.control_pts[2,:,:]) + (uniqueknots_y[2] - uniqueknots_y[1])
        self.domain[2].max = maximum(self.grid.control_pts[2,:,:]) - (uniqueknots_y[end] - uniqueknots_y[end-1])
        nx = size(grid.control_pts)[2]
        ny = size(grid.control_pts)[3]
        self.nodes = Array{Node,2}(nx,ny)

        # Simulation info
        self.timestep = simulation_info["timestep"]

        # Matrial info
        self.material_density = material_info["density"]
        self.g = material_info["gravity"]
        self.c_tait = material_info["c_tait"]
        self.k_bulkmod = material_info["k_bulkmod"]

        # Particles
        npartsx = particles_info["npartsx"]
        npartsy = particles_info["npartsy"]
        parts_xminfrac = particles_info["xminfrac"]
        parts_xmaxfrac = particles_info["xmaxfrac"]
        parts_yminfrac = particles_info["yminfrac"]
        parts_ymaxfrac = particles_info["ymaxfrac"]

        self.nparts = npartsx*npartsy
        self.parts = Array{Particle}(npartsx*npartsy)
        partsxmin = self.domain[1].min + (self.domain[1].max-self.domain[1].min)*parts_xminfrac
        partsxmax = self.domain[1].min + (self.domain[1].max-self.domain[1].min)*parts_xmaxfrac
        partsymin = self.domain[2].min + (self.domain[2].max-self.domain[2].min)*parts_yminfrac
        partsymax = self.domain[2].min + (self.domain[2].max-self.domain[2].min)*parts_ymaxfrac
        parts_hx = (partsxmax - partsxmin)/npartsx
        parts_hy = (partsymax - partsymin)/npartsy

        # Initialize Nodes
        for i = 1:nx
            for j = 1:ny
                self.nodes[i,j] = Node([i,j],self.grid,self.timestep)
            end
        end

        # Initialize Particles
        for i = 1:npartsx
            for j = 1:npartsy
                xloc = partsxmin + (i-1)*parts_hx + 0.5*parts_hx
                yloc = partsymin + (j-1)*parts_hy + 0.5*parts_hy
                pindex = (j-1)*npartsx + (i-1) + 1
                vol = parts_hx*parts_hy
                m0 = vol*self.material_density
                q0 = [0.0,0.0] # initial momentum
                stress0 = zeros(2,2) #initial stress
                F0 = eye(2,2) # iniital deformation gradient
                self.parts[pindex] = Particle( pindex,
                                               [xloc,yloc],
                                               q0,
                                               m0,
                                               vol,
                                               vol,
                                               stress0,
                                               F0,
                                               self.g,
                                               self.c_tait,
                                               self.k_bulkmod,
                                               self.grid.nnodesperknotspan,
                                               self.timestep,
                                               self.domain)
            end
        end
        return self
    end
end
function partXArr(self::IGMPMGrid2D)
    X = zeros(2,self.nparts)
    for (i,part) in enumerate(self.parts)
        X[:,i] = part.x
    end
    return X
end
function step(self::IGMPMGrid2D)
    # Set all the node values to zero
    for node in self.nodes
        node_zero_out(node)
    end
    # Add the contribution of each particle to the nodes
    for part in self.parts
        # Update the particle's info on the backgroud nodes of the element it's in.
        e,xi = e_xi_for_t(self.grid,part.x)
        BASISVALS = basis_tensorprod(self.grid,e,xi)
        DELBASISVALS = delBasis_tensorprod(self.grid,e,xi)
        for a1 = 1:self.grid.nnodesperknotspan[1]
            for a2 = 1:self.grid.nnodesperknotspan[2]
                node_i = self.grid.IEN[1][a1,e[1]] #x
                node_j = self.grid.IEN[2][a2,e[2]] #y
                part.elementnodes[a1,a2] = self.nodes[node_i,node_j]
                begin
                    #part.basisvals[a1,a2] = basis(self.grid,e,[a1,a2],xi)
                    #println("part.basisvals[a1,a2] = ",part.basisvals[a1,a2])
                    #println("BASISVALS[a1,a2] =      ",BASISVALS[a1,a2])
                    part.basisvals[a1,a2] = BASISVALS[a1,a2]

                    #part.del_basisvals[:,a1,a2] = delBasis(self.grid,e,[a1,a2],xi)[:]
                    #println("part.del_basisvals[:,a1,a2] = ",part.del_basisvals[:,a1,a2])
                    #println("DELBASISVALS[:,a1,a2] =       ",DELBASISVALS[:,a1,a2])
                    part.del_basisvals[:,a1,a2] = DELBASISVALS[:,a1,a2]
                end
            end
        end
        # map the particle data to the nodes
        particle_map_to_nodes(part)
    end
    # Do Explicit-Dynamic FEA step on the nodes
    for node in self.nodes
        node_move(node)
    end
    # Map change in values back to the particles, and
    #        update particle locations, velocities, etc
    for part in self.parts
        particle_map_from_nodes_and_move(part)
    end
end





# ***********************************************
# ***********************************************
# ***********************************************
# ***********************************************
# ***********************************************
# ***********************************************
# ***********************************************
# ***********************************************
# ***********************************************
# ***********************************************
# ***********************************************
# ***********************************************
# ***********************************************
# ***********************************************
# ***********************************************
# ***********************************************




# Building the background grid's Nurbs object
dim_p = 2
dim_s = 2
p = [3,3]
n = [4,4]
spans = n-1
knot_v = Array[[0., 0, 0, 0, 0.1, 0.1, 0.1, 0.1],[0., 0, 0, 0, 0.1, 0.1, 0.1, 0.1]]
c_pts = zeros(dim_s,n[1],n[2])
nurbsxmin = minimum(knot_v[1])
nurbsxmax = maximum(knot_v[1])
nurbsymin = minimum(knot_v[2])
nurbsymax = maximum(knot_v[2])
nurbs_hx = (nurbsxmax-nurbsxmin)/spans[1]
nurbs_hy = (nurbsymax-nurbsymin)/spans[2]
gridymin = 3
for i = 1:4
    for j = 1:4
        cpx = nurbsxmin + (i-1)*nurbs_hx
        cpy = nurbsymin + (j-1)*nurbs_hy
        c_pts[:,i,j] = [cpx,cpy]
    end
end

weights = Array{Array{Float64}}(2)
weights[1] = Array{Float64}(ones(n[1]))
weights[2] = Array{Float64}(ones(n[2]))
nurbs = Nurbs(dim_p, dim_s, p, knot_v, spans, c_pts, weights; use_value_dictionaries=false)
nelem_x = 14
nelem_y = 10
RefineMesh(nurbs,[nelem_x,nelem_y])
# Create bezier boundary
for dir = 1:2
    knot_to_insert = UniqueKnotArray(nurbs.knot_v[dir])[2]
    while(Multiplicity(nurbs.knot_v[dir],knot_to_insert) < nurbs.order[dir])
        InsertKnot(nurbs,dir,knot_to_insert)
    end
    knot_to_insert = UniqueKnotArray(nurbs.knot_v[dir])[end-1]
    while(Multiplicity(nurbs.knot_v[dir],knot_to_insert) < nurbs.order[dir])
        InsertKnot(nurbs,dir,knot_to_insert)
    end
end

uniqueknots_x = UniqueKnotArray(nurbs.knot_v[1])
uniqueknots_y = UniqueKnotArray(nurbs.knot_v[2])
KNOT_PTS_X = Array{Float64}([uniqueknots_x[i] for i = 1:length(uniqueknots_x), j=1:length(uniqueknots_y)])
KNOT_PTS_Y = Array{Float64}([uniqueknots_y[j] for i = 1:length(uniqueknots_x), j=1:length(uniqueknots_y)])

simulation_info = Dict()
hcell_x = (maximum(nurbs.control_pts[1,:,:]) - minimum(nurbs.control_pts[1,:,:]) / nelem_x)
hcell_y = (maximum(nurbs.control_pts[2,:,:]) - minimum(nurbs.control_pts[2,:,:]) / nelem_y)
hcell = minimum([hcell_x,hcell_y])
material_density = 1000.0 #kg/m^3
k_bulkmod = 15000.0 # Pa
c_tait = 0.0894
gravity = [0.0,-9.8] #m/s^2
simulation_info["timestep"] = 0.5*0.0125*hcell/sqrt(abs(k_bulkmod/material_density))

particles_info = Dict()
particles_info["npartsx"] = 12
particles_info["npartsy"] = 8
particles_info["xminfrac"] = 0.5
particles_info["xmaxfrac"] = 1.0
particles_info["yminfrac"] = 0.0
particles_info["ymaxfrac"] = 0.5

material_info = Dict()
material_info["density"] = material_density
material_info["gravity"] = gravity
material_info["k_bulkmod"] = k_bulkmod
material_info["c_tait"] = c_tait


self = IGMPMGrid2D(nurbs,simulation_info,material_info,particles_info)

println("So far so good?")
using PyPlot; plt = PyPlot;

loops_per_frame = 5
for i = 1:2000
    println("step ",i)
    step(self)
    if(loops_per_frame > 0 && mod(i,loops_per_frame)==1)
        X = partXArr(self)
        plt.figure()
        plt.scatter(KNOT_PTS_X,KNOT_PTS_Y,color="r")
        #plt.scatter(self.grid.control_pts[1,:],self.grid.control_pts[2,:],color="r")
        plt.hold(true)
        plt.scatter(X[1,:],X[2,:],color="b")
        plt.hold(false)
        plt.ylim([-0.01,0.11])
        plt.xlim([-0.01,0.11])
        plt.savefig("frames/frame"*lpad(i,5,0)*".png")
        plt.close()
    end
end

X = partXArr(self)
plt.scatter(X[1,:],X[2,:],color="b")
plt.hold(true)
plt.scatter(self.grid.control_pts[1,:],self.grid.control_pts[2,:],color="r")
plt.hold(false)
