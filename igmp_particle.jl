




# Library for NURBS
if(!isdefined(:SSNurbsTools))
    include("./ssnurbstoolsjl/nurbstools.jl")
end
include("./igmp_node.jl")

using SSNurbsTools
import SSNurbsTools.Nurbs
import SSNurbsTools.UniqueKnotArray
import SSNurbsTools.basis
import SSNurbsTools.basis_ds
import SSNurbsTools.delBasis
import SSNurbsTools.delBasis_ds
import SSNurbsTools.bernstein_basis_vdv
import SSNurbsTools.domain_dXdxi
import SSNurbsTools.domain_X
import SSNurbsTools.ConvertToBernsteinMesh
import SSNurbsTools.RefineMesh
import SSNurbsTools.Jacobian
import SSNurbsTools.Jacobian_ds

type Particle
    # Position
    x::Array{Float64}
    # Original position at init-time
    x0::Array{Float64}
    # Momentum
    q::Array{Float64}
    # Mass
    m::Float64
    # Volume
    vol::Float64
    restvol::Float64
    # Stress (2x2 matrix)
    stress::Array{Float64,2}
    # Deformation gradient (2x2 matrix)
    F::Array{Float64}
    # gradient of velocity
    gradv::Array{Float64,2}
    # Acceleration of gravity (for external force calculation)
    g::Array{Float64}
    c_tait::Float64
    k_bulkmod::Float64
    # Matrix of the node objects
    elementnodes::Array{Node,2}
    # Matrix containing the basis function values for each node
    basisvals::Array{Float64,2}
    # Matrix containing the derivative of the basis functions associated with each node
    del_basisvals::Array{Float64,3}
    # timestep
    timestep::Float64
    # domain
    domain::Array{Domain}

    # Init method
    function Particle(p_index::Int64,
                      x0::Array{Float64},
                      q0::Array{Float64},
                      m0::Float64,
                      vol0::Float64,
                      restvol0::Float64,
                      stress0::Array{Float64},
                      F0::Array{Float64},
                      g::Array{Float64},
                      c_tait::Float64,
                      k_bulkmod::Float64,
                      nnodesperel::Array{Int64},
                      timestep::Float64,
                      domain::Array{Domain})
        self = new()
        self.x0 = x0
        self.x = x0
        self.q = q0
        self.m = m0
        self.vol = vol0
        self.restvol = restvol0
        self.stress = stress0
        self.F = F0
        self.gradv = zeros(2,2)
        self.g = g
        self.c_tait = c_tait
        self.k_bulkmod = k_bulkmod
        self.elementnodes = Array{Node}(nnodesperel[1],nnodesperel[2])
        self.basisvals = zeros(nnodesperel[1],nnodesperel[2])
        self.del_basisvals = zeros(2,nnodesperel[1],nnodesperel[2])
        self.timestep = timestep
        self.domain = domain
        return self
    end
end
function constitutive_model(self)
    pressure = self.c_tait * self.k_bulkmod * (exp((1 - det(self.F))/self.c_tait) - 1)
    return Array{Float64,2}([-pressure 0; 0 -pressure])
end
function particle_map_to_nodes(self::Particle)
    # Map values to the nodes
    for a1 = 1:size(self.elementnodes)[1]
        for a2 = 1:size(self.elementnodes)[2]
            node = self.elementnodes[a1,a2]
            basisval = self.basisvals[a1,a2]
            del_basisval = self.del_basisvals[:,a1,a2]
            node.q += basisval * self.q
            node.m += basisval * self.m
            node.vol += basisval * self.vol
            # [x x] = [delbasis delbasis] * [sxx sxy] * vol     <-- matrix-vector multiplication
            #                               [syx sxx]
            node.fint += transpose( (-1) * transpose(del_basisval) * self.stress * self.vol )
            node.fext += basisval * self.g * self.m
        end
    end
    return
end
function outside_domain(self::Particle,dir::Int64,value::Float64)
    # Returns (is_outside,outside_edge,in_direction)
    if(value < self.domain[dir].min)
        return true,self.domain[dir].min,1
    end
    if(value > self.domain[dir].max)
        return true,self.domain[dir].max,-1
    end
    return false,0.0,1
end
function particle_map_from_nodes_and_move(self::Particle)

    # Zero-out the gradv before mapping it
    self.gradv[:] = 0.0

    # Loop over each node and add it's contribution to this particle
    for a1 = 1:size(self.elementnodes)[1]
        for a2 = 1:size(self.elementnodes)[2]
            node = self.elementnodes[a1,a2]
            if(node.m > 1e-14)
                basisval = self.basisvals[a1,a2]
                del_basisval = self.del_basisvals[:,a1,a2]
                self.gradv[1,1] += del_basisval[1] * (node.q[1]/node.m)
                self.gradv[1,2] += del_basisval[2] * (node.q[1]/node.m)
                self.gradv[2,1] += del_basisval[1] * (node.q[2]/node.m)
                self.gradv[2,2] += del_basisval[2] * (node.q[2]/node.m)

                # Update particle velocity
                self.q += self.m * basisval * (node.dqdt/node.m) * self.timestep

                # Update particle position
                self.x += basisval * (node.q/node.m) * self.timestep
            end
        end
    end

    # If the particle has moved outside the domain, then
    # put it right on the boundary and set that direction's
    # component velocity to zero.
    (out_x,x_edge,in_dir_x) = outside_domain(self,1,self.x[1])
    if(out_x)
        self.x[1] = x_edge + in_dir_x*(1e-11)
        self.q[1] = 0.0
    end
    (out_y,y_edge,in_dir_y) = outside_domain(self,2,self.x[2])
    if(out_y)
        self.x[2] = y_edge + in_dir_y*(1e-11)
        self.q[2] = 0.0
    end

    # Update the particle deformation gradient
    # F = ( [1 0] + dt * [gradx_vx gradx_vy] ) * [F00 F01]
    #     ( [0 1]        [grady_vx grady_vy] )   [F10 F11]
    self.F = (eye(2,2) + self.timestep * self.gradv) * self.F

    # If the particle is inverted, then reset the deformation gradient.
    if(det(self.F)<0.0)
        self.F = eye(2,2)
    end

    # Update the particle volumes
    self.vol = det(self.F)*self.restvol

    # Update the particle stress (Constitutive Model)
    self.stress = constitutive_model(self)
    return
end
