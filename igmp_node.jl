
# Library for NURBS
if(!isdefined(:SSNurbsTools))
    include("./ssnurbstoolsjl/nurbstools.jl")
end

using SSNurbsTools
import SSNurbsTools.Nurbs
import SSNurbsTools.UniqueKnotArray
import SSNurbsTools.basis
import SSNurbsTools.basis_ds
import SSNurbsTools.delBasis
import SSNurbsTools.delBasis_ds
import SSNurbsTools.bernstein_basis_vdv
import SSNurbsTools.domain_dXdxi
import SSNurbsTools.domain_X
import SSNurbsTools.ConvertToBernsteinMesh
import SSNurbsTools.RefineMesh
import SSNurbsTools.Jacobian
import SSNurbsTools.Jacobian_ds

type Domain
    min::Float64
    max::Float64
    function Domain(min,max)
        self = new()
        self.min = min
        self.max = max
        return self
    end
end

type Node

    # Control point index
    cp_i::Array{Int64}
    # Pointer to Nurbs background grid
    grid::Nurbs
    # "Lumped" Momentum
    q::Array{Float64}
    # "Lumped" Mass
    m::Float64
    # "Lumped" Volume
    vol::Float64
    # internal force
    fint::Array{Float64}
    # external force
    fext::Array{Float64}
    # m_i * a_i = dqdt
    dqdt::Array{Float64}
    # The time-step
    timestep::Float64

    # Initializer
    function Node(cp_i::Array{Int64},
                  grid::Nurbs,
                  timestep::Float64)

        self = new()
        self.cp_i = cp_i
        self.grid = grid

        self.q = zeros(2)
        self.m = 0.0
        self.vol = 0.0
        self.fint = zeros(2)
        self.fext = zeros(2)
        self.dqdt = zeros(2)

        return self
    end
end
function node_zero_out(self::Node)
    self.q[:] = 0.0
    self.m = 0.0
    self.vol = 0.0
    self.fint[:] = 0.0
    self.fext[:] = 0.0
    self.dqdt[:] = 0.0
    return
end
function node_move(self::Node)
    self.dqdt = (self.fint+self.fext)
    self.q = self.q + self.dqdt * self.timestep

    # Enforce Boundary Condition
    # If the node is on a boundary, then set
    # the perpendicular component of q to zero.
    nnodes_x = self.grid.spans[1]+1
    nnodes_y = self.grid.spans[2]+1
    if(self.cp_i[1] <= self.grid.order[1]+1)
        # x = 0 (left side of domain)
        self.q[1] = 0.0
        self.dqdt[1] = 0.0
    elseif(self.cp_i[1] >= nnodes_x-self.grid.order[1])
        # x = right side of domain
        self.q[1] = 0.0
        self.dqdt[1] = 0.0
    elseif(self.cp_i[2] <= self.grid.order[2]+1)
        # y = 0 (bottom of domain)
        self.q[2] = 0.0
        self.dqdt[2] = 0.0
    elseif(self.cp_i[2] >= nnodes_y-self.grid.order[2])
        # y = top of domain
        self.q[2] = 0.0
        self.dqdt[2] = 0.0
    end
    return
end
