
README for 'ssigmpm'

The Material Point Method has traditionally used a standard Finite Element
background mesh. This project is meant to validate the basic idea of a
MPM that uses, instead, the NURBS basis functions.

Particle object
Node object (one for every control point)
Nurbs object: (contains the background mesh information, the control point
              locations, the basis function functions (for evaluating
              the basis functions with), etc.)

NOTE: While usually the NURBS has its own parameterization which is different
than spatial coordinates, in this project we are keeping the two the same, so
that (t1,t2) is always the same as (x,y).  This is what I'll do for now, but
later I'll have to write a way to compute the (t1,t2) from the (x,y).  This is
a backwards direction from where we usually go in FEA-- and might require a few
iterations to converge on the right value.  Efficiency concerns are definitely
something worth discussing, for that).


TODO: (Nov 17, 2016)
I think I know what needs to happen next
The problem is, I am not setting the boundary conditions properly.
I need to have a "bezier end condition", which means I put repeated knots near
the boundary so the last element is an independent bezier curve and lets me
properly control the boundary conditions.
------> (I AM DOING THIS! We'll see how it goes)

ALSO:
I think the problem might be that I'm switching row/col vs x/y order somewhere...
------> I think I fixed this.

End of day Nov 17, 2016:
Currently the particles just fall down into nothing (I've seen that before).
What's wrong?
--->Kind of fixed it!  It was the row/col vs x/y problem.  I fixed that.

Very very end of Nov 17, 2016:
Things are starting to work! But not quite all the way.  Stray particles leaking
are causing hiccups.
Todo: Plot the element boundaries so it's easier to see!
What to do with stray particles?
Work on an algorithm to easily put exactly integer-2-parts-per-cell or
whatever number.


-----------------------------
-----------------------------
-----------------------------
Future research on IGMPM:
* The boundary error problem
* The 'integer-number-of-particles-per-cell' problem solved now?
* No more cell-crossing errors with NURBS background grid?
